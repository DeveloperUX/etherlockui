import { merge } from 'lodash';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import '../css/crypto-palette.css';
const colors = require('material-ui/styles/colors');

const cryptoColors = {
  sapphire: '#052564',
  lochmara: '#2065A2',
  bondi: '#0284BE',
  pacific: '#019DCA',
  rock: '#8E98AF',
  turquoise: '#1AE1EE',
  red: '#D50000',
};

let theme = {
  borderRadius: 2,
  fontFamily: 'Rajdhani, sans-serif',
  palette: {
    textColor: colors.white,
    alternateTextColor: cryptoColors.red,  // Text on Snackbar
    canvasColor: cryptoColors.lochmara, // Card background color
    primary1Color: cryptoColors.turquoise,  // Button text color
    primary2Color: colors.white,
    primary3Color: colors.white,  // Slider right side color
    accent1Color: colors.white,
    accent2Color: colors.white,
    accent3Color: colors.white,
  },
  appBar: {
    color: cryptoColors.sapphire,
    height: 100,
  }
};

theme = merge({}, darkBaseTheme, theme);

export default theme;