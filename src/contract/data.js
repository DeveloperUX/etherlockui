import WEB3 from './metamask';

const CONTRACT_ADDRESS = '0x6d4e3af8d766cdea9307fb266ba538cc322b91fa';

// Set up the Ethereum Contract and everything that'll be needed

const ABI = [{
  "constant": true,
  "inputs": [],
  "name": "getUnlockTimestamp",
  "outputs": [{"name": "", "type": "uint256"}],
  "payable": false,
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "getContractAddress",
  "outputs": [{"name": "", "type": "address"}],
  "payable": false,
  "type": "function"
}, {
  "constant": false,
  "inputs": [],
  "name": "withdraw",
  "outputs": [],
  "payable": false,
  "type": "function"
}, {
  "constant": true,
  "inputs": [{"name": "sender", "type": "address"}],
  "name": "getAvailableBalance",
  "outputs": [{"name": "bal", "type": "uint256"}],
  "payable": false,
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "isLocked",
  "outputs": [{"name": "", "type": "bool"}],
  "payable": false,
  "type": "function"
}, {
  "constant": false,
  "inputs": [{"name": "futureTime", "type": "uint256"}, {"name": "penaltyPercent", "type": "uint256"}],
  "name": "deposit",
  "outputs": [],
  "payable": true,
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "getPenalty",
  "outputs": [{"name": "", "type": "uint256"}],
  "payable": false,
  "type": "function"
}, {
  "constant": false,
  "inputs": [{"name": "sender", "type": "address"}],
  "name": "getBalance",
  "outputs": [{"name": "", "type": "uint256"}],
  "payable": false,
  "type": "function"
}, {
  "inputs": [{"name": "_splitterContract", "type": "address"}],
  "payable": false,
  "type": "constructor"
}, {"payable": true, "type": "fallback"}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "name": "addr", "type": "address"}, {
    "indexed": false,
    "name": "amount",
    "type": "uint256"
  }],
  "name": "LogDeposit",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "name": "addr", "type": "address"}, {
    "indexed": false,
    "name": "amount",
    "type": "uint256"
  }],
  "name": "LogSplit",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "name": "addr", "type": "address"}, {
    "indexed": false,
    "name": "amount",
    "type": "uint256"
  }],
  "name": "LogWithdraw",
  "type": "event"
}];
const EtherLockContract = WEB3.eth.contract(ABI);
const contract = EtherLockContract.at(CONTRACT_ADDRESS);

export default contract;