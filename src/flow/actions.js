export function withdrawMoney(txHash) {
  return {
    type: 'BEGIN_WITHDRAW',
    payload: txHash,
  }
}

export function depositMoney(txHash) {
  return {
    type: 'BEGIN_DEPOSIT',
    payload: txHash,
  }
}

export function canWithdraw(info) {
  return {
    type: 'CAN_WITHDRAW',
    payload: {
      penalty: 0,
    }
  }
}

export function cannotWithdraw(info) {
  return {
    type: 'CANNOT_WITHDRAW',
    payload: {
      penalty: info.penalty,
    }
  }
}

export function enterWithdrawScreen(depositInfo) {
  return {
    type: 'ENTER_WITHDRAW_SCREEN',
    deposit: {...depositInfo},
  }
}