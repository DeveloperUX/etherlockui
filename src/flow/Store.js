import { applyMiddleware, combineReducers, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';


const initialDepositState = {
  readyToDeposit: false,
  inProgress: false,
  amount: 0,
  period: '',
  penalty: 5,
};

const initialWithdrawState = {
  hasBalance: false,
  balance: 0,
};

const depositReducer = (state=initialDepositState, actions) => {
  switch(actions.type) {
    case 'DEPOSIT_VALID': {
      state = {...state, ...actions.payload, readyToDeposit: true};
      break;
    }
    case 'DEPOSIT_INVALID': {
      state = {...state, readyToDeposit: false};
      break;
    }
    case 'MONEY_SENT': {
      state = {...state, inProgress: true, readyToDeposit: false};
      break;
    }
    default: {
      break;
    }
  }
  return state;
};

const withdrawReducer = (state=initialWithdrawState, actions) => {
  switch (actions.type) {
    case 'ENTER_WITHDRAW_SCREEN': {
      state = {...state, ...actions.deposit};
      break;
    }
    case 'FOUND_BALANCE': {
      state = {...state, ...actions.payload, hasBalance: true};
      break;
    }
    case 'CAN_WITHDRAW': {
      state = {...state, ...actions.payload, hasPenalty: false};
      break;
    }
    case 'CANNOT_WITHDRAW': {
      state = {...state, ...actions.payload, hasPenalty: true};
      break;
    }
    default: {
      break;
    }
  }
  return state;
};

const transactionReducer = (state={}, actions) => {
  switch (actions.type) {
    case 'BEGIN_DEPOSIT': {
      return {...state, inProgress: true, depositComplete: false, txHash: actions.payload.txHash};
    }
    case 'BEGIN_WITHDRAW': {
      return {...state, inProgress: true, withdrawComplete: false, txHash: actions.payload.txHash};
    }
    case 'DEPOSIT_COMPLETE': {
      return {...state, inProgress: false, depositComplete: true};
    }
    case 'WITHDRAW_COMPLETE': {
      return {...state, inProgress: false, withdrawComplete: true, block: actions.payload};
    }
    case 'WITHDRAW_FAILED': {
      return {...state, inProgress: false, withdrawComplete: false, notification: actions.payload.message};
    }
    case 'RESET_NOTIFICATIONS': {
      return {...state, notification: null};
    }
    default: {
      break;
    }
  }
  return state;
};

const reducers = combineReducers({
  deposit: depositReducer,
  withdraw: withdrawReducer,
  transaction: transactionReducer,
});

const middleWare = applyMiddleware(logger);

const store = createStore(reducers, middleWare);

export default store;