import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';

const dangerous = 'red';

class WithdrawButton extends Component {
  render() {
    return (
      <div className="deposit-link">
        { this.props.hasPenalty ? <div className="arrows" /> : '' }
        <FlatButton hoverColor={this.props.hasPenalty ? dangerous : ''}
                    label="Withdraw" primary={true}
                    onClick={this.props.actionToTake} />
      </div>
    )
  }
}

export default WithdrawButton;