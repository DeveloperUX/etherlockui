import React, { Component } from 'react';
import FontIcon from 'material-ui/FontIcon';

const iconStyle = {
  paddingRight: '12px',
  fontSize: '48px',
};

class Instructions extends Component {
  render() {
    return (
      <div className="Tutorial">
        <ul className="instructions-list text-white">
          <li>
            <FontIcon className="material-icons" style={iconStyle}>update</FontIcon>
            <span>Set the lock period</span>
          </li>
          <li>
            <FontIcon className="material-icons" style={iconStyle}>traffic</FontIcon>
            <span>Charge yourself a penalty for withdrawing early</span>
          </li>
          <li>
            <FontIcon className="material-icons" style={iconStyle}>exit_to_app</FontIcon>
            <span>Transfer your funds to the Contract</span>
          </li>
          <li>
            <FontIcon className="material-icons" style={iconStyle}>attach_money</FontIcon>
            <span>Funds are returned when lock period is over</span>
          </li>
        </ul>
      </div>
    )
  }
}

export default Instructions;