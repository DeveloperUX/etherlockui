import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import Slider from 'material-ui/Slider';
import moment from 'moment';
import Store from '../flow/Store';

const sliderStyle = {
  marginTop: '-12px',
  marginBottom: '-12px',
};


// The deposit form
class DepositForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      amountLabel: 'Amount To Lock Away',
      timeLength: 'Time Length',
      periodErrorText: '',
      amount: '',
      period: '',
      penalty: 5,
      errorText: '',
    };
    this.format = /^([0-9]+)\s(second|minute|hour|day|week|month|year)(s?)$/g;
  }

  changePeriod(event) {
    if (!event.target.value.match(this.format) ) {
      this.setState({
        periodErrorText: 'Bad format, try "50 days"',
        period: '',
      });
      return undefined;
    }

    const periodStr = event.target.value;
    const numeric = periodStr.split(' ')[0];
    const denom = periodStr.split(' ')[1];
    const futureDate = moment().add(numeric, denom);

    this.setState({
      timeLength: `Time Length (${futureDate.calendar()})`,
      period: futureDate,
      periodErrorText: '',
    });

    console.log('Future Date: ', futureDate.toDate());
  }

  // If anything updated, check if the form is complete and dispatch an event to show the Go! button
  componentDidUpdate(prevProps, prevState) {
    if (this.state.amount && this.state.penalty && this.state.period) {
      Store.dispatch({
        type: 'DEPOSIT_VALID',
        payload: {
          amount: this.state.amount,
          penalty: this.state.penalty,
          period: this.state.period,
        },
      });
    } else {
      Store.dispatch({type: 'DEPOSIT_INVALID'});
    }
  }


  render() {
    return (
      <form className="inputs-card text-white grid-wrapper">
        <TextField className="grid-amount"
          id="input-amount"
          hintText="20"
          floatingLabelText={this.state.amountLabel}
          type="number"
          min="0" max="10000"
          value={this.state.amount}
          onChange={(e) => this.setState({ amount: e.target.value})}
        />
        <TextField className="grid-period"
          id="input-period"
          hintText="3 weeks"
          floatingLabelText={this.state.timeLength}
          type="string"
          errorText={this.state.periodErrorText}
          onChange={this.changePeriod.bind(this)}
        />
        <div className="grid-penalty">
          <span>Penalty: {this.state.penalty}%</span>
          <Slider style={sliderStyle} min={5} max={50} step={1} axis="x" defaultValue={5}
                  onChange={(e, value) => this.setState({ penalty: value})} />
        </div>
      </form>
    )
  }
}

export default DepositForm;