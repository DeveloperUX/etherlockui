import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/reset.css';
import '../css/App.css';
import '../css/dashboard-palette.css';
import '../css/arrow.css';
import { AppBar, Snackbar } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FontIcon from 'material-ui/FontIcon';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import cryptoTheme from '../themes/cryptoTheme';
import moment from 'moment';

import DepositForm from './DepositForm';
import Instructions from './Instructions';
import Withdraw from './Withdraw';
import Loading from './Loading';
import ActionButton from './ActionButton';
import Store from '../flow/Store';

import WEB3 from '../contract/metamask';
import contract from '../contract/data';

console.log('Web3 Instance: ', WEB3);
console.log('User Address: ', WEB3.eth.coinbase);
console.log('Contract Object: ', contract);

const theme = getMuiTheme(cryptoTheme);


const logoStyle = {
  fontSize: '42px',
  color: 'white',
};

const appbarStyle = {
  boxShadow: 'none',
  backgroundColor: 'transparent',
  paddingLeft: '80px',
  color: 'white',
};

const GAS = 100000;

class App extends Component {
  constructor(props) {
    super(props);
    // this.WEB3;
    this.state = {
      address: '0xae8fcf9171fce56a9c1f96372430923ef8878468',
      timeLength: 'Time Length',
      amountLabel: 'Amount To Lock Away (In Eth)',
    };

    this.format = /^([0-9]+)\s(second|minute|hour|day|week|month|year)(s?)$/g;

    this.deposit = this.deposit.bind(this);
    this.handleCopyAddress = this.handleCopyAddress.bind(this);
  }

  componentDidMount() {
    // When the page loads check if any of the user's accounts have a balance already
    contract.getBalance.call(WEB3.eth.coinbase, (err, balance) => {
      if (balance.toNumber() > 0) {
        // User has a balance
        const tx = { from: WEB3.eth.coinbase, gas: GAS };
        contract.getUnlockTimestamp.call(tx, (err, time) => {
          const unlockTime = moment.unix(time.toNumber()).toDate();
          contract.getPenalty.call(tx, (err, penalty) => {
            console.log('unlock time: ', unlockTime);
            console.log('penalty: ', penalty.toNumber());
            Store.dispatch({
              type: 'FOUND_BALANCE',
              payload: {
                balance: WEB3.fromWei(balance.toNumber(), 'ether'),
                unlockTime: time.toNumber(),
                penalty: penalty.toNumber(),
                readyToWithdraw: (time.toNumber() - moment().unix()) <= 0,
              },
            });
          });
        });
      }
    })
  }

  handleCopyAddress = () => {
    console.log('copy to clipboard');
    const periodInput = document.getElementById('input-period');
    periodInput.select();
    document.execCommand('copy');
    this.setState({
      copied: true,
    });
  };

  deposit() {
    const today = moment();
    // The public address user is using now through MetaMask / Parity
    const account = WEB3.eth.coinbase;
    // Amount in Wei
    const amount = WEB3.toWei(this.props.amount, 'ether');
    // TODO: Calculate gas dynamically
    const tx = {
      from: account,
      gas: GAS,
      value: amount
    };
    console.log(`Future Date in Seconds: ${this.props.period} \n Current Time in Seconds: ${today}`);
    console.log('TX: ', tx);
    // Fire off the transaction
    contract.deposit(this.props.period.unix(), this.props.penalty, tx, ((err, txHash) => {
      console.log('Transaction Hash: ', txHash);
      const txPoll = setInterval(() => {
        WEB3.eth.getTransaction(txHash, (err, block) => {
          if (block && block.blockNumber) {
            clearInterval(txPoll);
            Store.dispatch({
              type: 'DEPOSIT_COMPLETE'
            })
          }
        })
      }, 1000);
      Store.dispatch({
        type: 'BEGIN_DEPOSIT',
        payload: {
          txHash: txHash,
        },
      });
    }));
  }

  handleRequestClose() {
    Store.dispatch({
      type: 'RESET_NOTIFICATIONS'
    });
  }

  render() {
    const title = <span className="app-name">WeiLock</span>;
    const logo = <FontIcon className="material-icons" style={logoStyle}>lock_open</FontIcon>;
    const depositOrWithdraw = () => {
      if (this.props.hasBalance || this.props.depositComplete) {
        return (
          <div className="App">
            <Withdraw unlockTime={this.props.unlockTime} amount={this.props.balance} penalty={this.props.penalty} />
            {/*<ActionButton hasPenalty={this.props.hasPenalty} actionToTake={this.withdraw} />*/}
          </div>
        )
      } else {
        return (
          <div className="App">
            <Instructions />
            <DepositForm />
            { this.props.readyToDeposit ? <ActionButton actionToTake={this.deposit} /> : ''}
          </div>
        )
      }
    };

    return (
      <MuiThemeProvider muiTheme={theme}>
        <div className="Wrapper">
          <AppBar title={title} style={appbarStyle} iconElementLeft={logo} />
          {
            this.props.inProgress ? <Loading /> : depositOrWithdraw()
          }
          {
            this.props.notification &&
            <Snackbar
              open={!!this.props.notification}
              message={this.props.notification.message}
              autoHideDuration={4000}
              onRequestClose={this.handleRequestClose.bind(this)}
            />
          }
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('map: ', state);
  let notification ;
  if (state.transaction.notification) {
    notification = {
      message: state.transaction.notification,
    };
  }
  return {
    readyToDeposit: state.deposit.readyToDeposit,
    inProgress: state.transaction.inProgress,
    amount: state.deposit.amount,
    penalty: state.deposit.penalty,
    period: state.deposit.period,
    hasBalance: state.transaction.withdrawComplete ? false : state.withdraw.hasBalance,
    balance: state.withdraw.balance,
    unlockTime: state.withdraw.unlockTime,
    readyToWithdraw: state.withdraw.readyToWithdraw,
    hasPenalty: state.withdraw.hasPenalty,
    depositComplete: state.transaction.depositComplete,
    withdrawComplete: state.transaction.withdrawComplete,
    notification: notification,
  }
};

const mapDispatchToProps = (dispatch) => {
  console.log('dispatch: ', dispatch);
  return {};
};

const AppConnected = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppConnected;
