import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import Store from '../flow/Store';
import { canWithdraw, cannotWithdraw, enterWithdrawScreen } from '../flow/actions';
import { Card, CardText, CardTitle,   Table, TableBody, CardActions, TableRow, TableRowColumn, FlatButton } from 'material-ui';
import WithdrawButton from "./WithdrawButton";

import WEB3 from '../contract/metamask';
import contract from '../contract/data';

const cardTitleStyle = {
  justifyContent: 'center',
  display: 'flex'
};

// Fix for Card Actions
const cardActionsStyle = {
  padding: '16px',
  display: 'flex',
  justifyContent: 'end'
};

class Withdraw extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countdownText: '',
    };
    this.withdraw = this.withdraw.bind(this);
  }

  withdraw() {
    // The public address user is using now through MetaMask / Parity
    const account = WEB3.eth.coinbase;
    // TODO: Calculate gas dynamically
    const tx = {
      from: account,
      gas: 100000,
    };
    console.log('Begin Withdraw TX: ', tx);
    // Fire off the transaction to withdraw
    contract.withdraw(tx, ((err, txHash) => {

      Store.dispatch({
        type: 'BEGIN_WITHDRAW',
        payload: {
          txHash: txHash,
        },
      });

      if (txHash) {
        console.log('Withdraw TX Hash: ', txHash);
        const txPoll = setInterval(() => {
          WEB3.eth.getTransaction(txHash, (err, block) => {
            if (block && block.blockNumber) {
              console.log('BLOCK: ', block);
              clearInterval(txPoll);
              Store.dispatch({
                type: 'WITHDRAW_COMPLETE',
                payload: block,
              });
            }
          })
        }, 1000);
      } else if (err) {
        console.log('Error Withdrawing: ', err);
        if (err.message.includes('User denied transaction signature')) {
          Store.dispatch({
            type: 'WITHDRAW_FAILED',
            payload: err,
          });
        }
      }
    }));
  }

  getUnlockTime(cb) {
    const FROM_USER = { from: WEB3.eth.coinbase };
    contract.getUnlockTimestamp.call(FROM_USER, cb);
  }

  componentWillMount() {
    this.getUnlockTime((err, unlockTime) => {
      // Visiting the Withdraw screen, make sure we grab the user's
      // current MetaMask address and unlock time for their deposit
      const accountInfo = {
        address: WEB3.eth.coinbase,
        unlockTime: moment.unix(unlockTime).add(12, 'seconds').unix(),
      };
      console.log('UNLOCK TIME: ', unlockTime);
      Store.dispatch(enterWithdrawScreen(accountInfo));
    });
  }

  componentDidMount() {
    // update the counter every second
    const INTERVAL = 1;
    const checkTimeLeft = () => {
      this.getUnlockTime((err, unlockTime) => {
        // Get the time difference in seconds
        const trueUnlockTime = moment.unix(unlockTime).add(12, 'seconds'); // seconds until next block found
        // Get the time remaining until the unlock period
        const timeDiff = trueUnlockTime.unix() - moment().unix();
        console.log(`time left: ${timeDiff} seconds`);
        if (timeDiff <= 0) {
          clearInterval(counter);
          Store.dispatch(canWithdraw({}));
        } else {
          Store.dispatch(cannotWithdraw({
            penalty: this.props.penalty,
          }));
        }
      });
    };

    checkTimeLeft();

    const counter = setInterval(checkTimeLeft, INTERVAL * 10000)
  }

  render() {
    let afterPenalty = 0;
    let countdownText = 'You Can Now Withdraw Without Penalty!';
    let penaltyView;
    let leftOverView;

    if (this.props.hasPenalty) {
      countdownText = 'Funds will be available ' + moment.unix(this.props.unlockTime).fromNow();
      afterPenalty = this.props.amount - (this.props.penalty * 0.01 * this.props.amount);
      penaltyView = (
          <TableRow>
            <TableRowColumn>Penalty</TableRowColumn>
            <TableRowColumn>{ this.props.penalty }%</TableRowColumn>
          </TableRow>
        );
      leftOverView = (
          <TableRow>
            <TableRowColumn>Amount After Penalty</TableRowColumn>
            <TableRowColumn>{ afterPenalty }</TableRowColumn>
          </TableRow>
      )
    }

    return (
      <Card className="account-info-card countdown-timer text-white">
        <CardTitle title={countdownText} style={cardTitleStyle}/>
        <CardText>
          <div className="deadcenter">{ this.props.amount } Ether</div>
        </CardText>
        <CardText>
          <Table className="withdraw-info-table" selectable={false}>
            <TableBody displayRowCheckbox={false}>
              {penaltyView}
              {leftOverView}
            </TableBody>
          </Table>
        </CardText>
        <CardActions style={cardActionsStyle}>
          <FlatButton hoverColor={this.props.hasPenalty ? 'red' : ''}
                      label="Withdraw" primary={true}
                      onClick={this.withdraw} />
        </CardActions>
      </Card>
    )
  }
}

const mapStateToProps = (state) => {
  const props = {
    hasPenalty: state.withdraw.hasPenalty,
    penalty: state.deposit.penalty,
  };
  if (state.withdraw.deposit) {
    props.curAddress = state.withdraw.address;
    props.unlockTime = state.withdraw.unlockTime;
  }
  return props;
};

const mapDispatchToProps = (dispatch) => {
  console.log('dispatch: ', dispatch);
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Withdraw);