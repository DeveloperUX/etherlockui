import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';

const dangerous = 'red';

class ActionButton extends Component {
  render() {
    return (
      <div className="deposit-link">
        { this.props.hasPenalty ? <div className="arrows" /> : '' }
        <FlatButton hoverColor={this.props.hasPenalty ? dangerous : ''} className="grid-go-btn" label="Go" primary={true} onClick={this.props.actionToTake} />
      </div>
    )
  }
}

export default ActionButton;