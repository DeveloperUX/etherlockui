import React, { Component } from 'react';
import '../css/loading.css';

// Taken from: http://www.ajerez.es/en/

class Loading extends Component {
  render() {
    return (
      <div className="loading">
        <div className="sea">
          <div className="circle-wrapper">
            <div className="bubble"></div>
            <div className="submarine-wrapper">
              <div className="submarine-body">
                <div className="window"></div>
                <div className="engine"></div>
                <div className="light"></div>
              </div>
              <div className="helix"></div>
              <div className="hat">
                <div className="leds-wrapper">
                  <div className="periscope"></div>
                  <div className="leds"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="text-white text-large loading-text saving">Almost There<span>.</span><span>.</span><span>.</span></div>
      </div>
    )
  }
}

export default Loading;